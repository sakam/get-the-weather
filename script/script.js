(function() {

	var app = angular.module('weather', []);

	app.controller("weatherController", function() {
		
		var appiKey = "appid=b3b1c46ae89797a3fb4e5afdbec313eb";

		document.getElementById("tbInput").addEventListener("keyup", function(event) {
			if (event.keyCode == 13) {
				var grad = $("#tbInput").val();
				var gradovi = grad.split(",");
				for(var i = 0; i < gradovi.length; i++) {
					var url = "http://api.openweathermap.org/data/2.5/weather?q="+gradovi[i]+"&"+appiKey;
					$.getJSON(url).done(prikaziVreme);
				}
			}
		});

		function prikaziVreme(data){
			var grad = data.name;  
			var temp = data.main.temp - 273.15; 
			var stanje = data.weather[0].main;
			var vlaznost = data.main.humidity + "%";
			var vetar = data.wind.speed * 3.6;
			var newDate = new Date();
			var date = newDate.toLocaleDateString();
			var $container = $("#data");
			

			var $div1 = $("<div class='divRow'><span class='city'>" + grad + "</span></br>" + date + "</div>"); 
			var $div2 = $("<div class='divRow'><span class='temperature'>" + temp.toFixed(0) + " <sup><sup>o</sup>c</sup></span>" + "</br>" + stanje + "</div>");
			var $div3 = $("<div class='divRow'>" + "Humidity: " + vlaznost + "</br>" + "Wind: " + vetar.toFixed(2) + "km/h" + "</div>");
			$container.append($div1, $div2, $div3);
		}
	});
})();